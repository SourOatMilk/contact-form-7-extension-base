#!/usr/bin/env python

# Standalone plugin builder for CF7 Extension Base
# Copyright (C) 2019 SourOatMilk (email: souroatmilk@protonmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This is a standalone plugin builder for CF7 Extension Base. It merges the
extension build on top of CF7EB with the framework itself.

TODO: There are steps that should be smaller for readability. Some of them also
do more than what the method name suggests. Comments are poor and won't really
explain all the important details. Heavy refactoring is indeed needed.
"""
import argparse
import os
import shutil
import errno
import json
import re
import sys


class colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class Standalone():
    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Standalone CF7 Extension builder')
        parser.add_argument(
            '-e',
            '--extension',
            help='Path to extension to make standalone of.',
            dest='extension',
            required=True)
        parser.add_argument(
            '-n',
            '--namespace',
            help='New namespace, will be suffixed with `Standalone`. '
            'If `-t` is not passed, standalone will be created under '
            '`../[NAMESPACE]-standalone`.',
            dest='namespace',
            required=True)
        parser.add_argument(
            '-t',
            '--taget',
            help='Optional. Path to standalone extension.',
            dest='target')
        parser.add_argument(
            '-z',
            '--zip',
            help='Create ZIP file of standalone extension.',
            dest='zip',
            action='store_true')
        parser.add_argument(
            '-v',
            '--verbose',
            help='Verbose mode.',
            dest='verbose',
            action='store_true')
        parser.add_argument(
            '-y',
            '--yes',
            help='Say yes to all questions, what ever they may be!',
            dest='yes',
            action='store_true')
        args = parser.parse_args()

        self.ignored = [
            '.git', 'node_modules', '.env', 'vendor', 'webpack.config.js',
            'composer.lock', 'package.json', 'package-lock.json',
            'standalone.py', '__pycache__', 'README.md'
        ]
        self.ext = os.path.abspath(args.extension)
        self.ns = args.namespace + 'Standalone'
        self.ns_kepab = str.lower('{}-standalone'.format(args.namespace))
        self.yes = args.yes
        self.verbose = args.verbose
        self.zip = args.zip
        self.base = os.path.abspath('./')
        self.constants = {}
        self.remove_files = []
        self.entry_comments = ''
        self.base_ns = self.ext_ns = self.author = ''
        self.dst = args.target or '../' + self.ns_kepab

        self.build()

    def build(self):
        """Builds the standalone"""
        self.puts('Starting build', colors.HEADER)

        self.puts('Gathering paths')
        base_paths = self.get_paths(self.base)
        ext_paths = self.get_paths(self.ext)

        self.puts('Getting `composer.json` files')
        base_com = self.get_path_to(base_paths, 'composer.json')
        ext_com = self.get_path_to(ext_paths, 'composer.json')

        self.puts('Creating `composer.json` file')
        composer = json.dumps(
            self.merge_composer_files(base_com, ext_com), indent=2)

        try:
            self.puts('Creating `{}`'.format(self.dst))
            os.makedirs(self.dst)
        except OSError:
            msg = '!? `{}` exists, continue? [y/N]: '.format(self.dst)
            cont = input(colors.WARNING + msg +
                         colors.ENDC) if not self.yes else 'y'
            if cont in 'yY' and cont != '':
                self.puts('`{}` exists already, using it'.format(self.dst))
            else:
                return False

        self.puts('Copying files to "{}"'.format(self.dst))
        self.copy(base_paths, self.base)
        self.copy(ext_paths, self.ext)

        self.puts('Renaming namespace and constants')
        self.rename()

        self.puts('Cleaning up')
        self.cleanup()

        self.puts('Creating and writing entry file as `{}.php`'.format(
            self.ns_kepab))
        self.create_entry()

        self.puts('Writing `composer.json` file')
        with open(self.dst + '/' + 'composer.json', 'w') as composer_json:
            composer_json.write(composer)

        self.puts('Running `composer install`')
        self.install()

        if self.zip:
            self.puts('Creating a .zip file')
            self.zip_it()

        self.puts('All done!', colors.OKGREEN)

    def puts(self, msg, color=''):
        """Verbose messages"""
        _msg = ':: {}'.format(msg)

        if color:
            _msg = color + _msg + colors.ENDC

        if (self.verbose):
            print(_msg)

    def is_in(self, iterable, string):
        """Test if string is in list"""
        for item in iterable:
            if item in string:
                return True
        return False

    def get_paths(self, target):
        """Get all file paths from target"""
        paths = []
        for root, dirs, files in os.walk(target):
            if self.is_in(self.ignored, root):
                continue
            for file in files:
                if self.is_in(self.ignored, file):
                    continue
                path = os.path.abspath(root + '/' + file)
                paths.append(path)
        return paths

    def get_dict_key(self, d, v):
        """Get key from dict using value"""
        return list(d.keys())[list(d.values()).index(v)]

    def get_path_to(self, paths, file):
        """Get path to file from paths"""
        for path in paths:
            if file in path:
                return path

    def read_json(self, file):
        """Read JSON file"""
        data = None
        with open(file, 'r') as f:
            data = json.load(f)
        return data

    def update_merge(self, a, b):
        """Merges two dicts, only overriding items that are not dict or list"""
        if isinstance(a, dict) and isinstance(b, dict):
            return {
                **a,
                **b,
                **{
                    k: a[k] if a[k] == b[k] else self.update_merge(a[k], b[k])
                    for k in {*a} & {*b}
                }
            }
        if isinstance(a, list) and isinstance(b, list):
            return list(set([*a, *b]))

        return b

    def merge_composer_files(self, base, ext):
        """Merges composer.json files and modifies the result"""
        base_data = self.read_json(base)
        ext_data = self.read_json(ext)
        self.base_ns = self.get_dict_key(base_data['autoload']['psr-4'],
                                         ['src/'])

        ext_name = ext_data['name']
        self.ext_ns = ext_psr = self.get_dict_key(
            ext_data['autoload']['psr-4'], ['src/'])
        self.author = re.search(r'(\w+)\\', re.escape(self.ext_ns)).group(1)

        new_name = re.search('(.+)/', ext_name).group(0) + str.lower(self.ns)
        new_psr = re.search(r'^(\w+)', ext_psr).group(0) + \
            '\\' + self.ns + '\\'

        merged = self.update_merge(base_data, ext_data)
        new_data = {
            'name': new_name,
            'autoload': {
                'psr-4': {
                    new_psr: ['src/']
                }
            }
        }

        return {**merged, **new_data}

    def copy(self, srcs, root):
        """Copies files from sources"""
        dst = self.dst
        for src in srcs:
            try:
                shutil.copytree(
                    src, dst, ignore=shutil.ignore_patterns(*self.ignored))
            except OSError as exc:
                try:
                    path = str.replace(src, root + '/', '')
                    dpath = dst + '/' + path
                    os.makedirs(os.path.dirname(dpath))
                except OSError:
                    None
                if exc.errno == errno.ENOTDIR:
                    shutil.copy(src, dpath)
                else:
                    raise

    def rename(self):
        """Renames namespaces and constants"""
        src = self.dst
        constants = {}
        prefixed_constants = {}
        files_defining_constants = []
        php_files = []
        comments = []
        for root, dirs, files in os.walk(src):
            for file in files:
                if '.php' in file:
                    php_files.append(os.path.abspath(root + '/' + file))

        for file in php_files:
            lines = []
            with open(file) as original_file:
                file_defined_constants = False
                is_ext_entry = os.path.exists(self.ext + '/' +
                                              os.path.basename(file))
                for line in original_file:
                    const_match = re.search('define\(.(.+)., (.+)\);', line)
                    com_match = re.search(r'^\s*?(/\*|\*|\*/)', line)

                    if const_match:
                        file_defined_constants = True
                        const = const_match.group(1)
                        val = const_match.group(2)
                        constants.update({const: val})
                        prefixed_constants.update({
                            str.upper(self.ns) + '_' + const:
                            val
                        })

                    if is_ext_entry:
                        if com_match:
                            comments.append(line)

                    lines.append(line)

                if file_defined_constants:
                    files_defining_constants.append(file)

            with open(file, 'w') as new_file:
                re_consts = re.compile('|'.join(c for c in constants))

                for line in lines:
                    line_out = line
                    ns_matches = re.search(r'(\w+)\\(\w+)\\', line_out)
                    re_const = re.search(re_consts, line_out)

                    if re_const:
                        c = re_const.group(0)
                        line_out = re.sub(c,
                                          str.upper(self.ns) + '_' + c,
                                          line_out)

                    if ns_matches:
                        m = re.escape(ns_matches.group(0))
                        n = re.escape(
                            ns_matches.group(1) + '\\' + self.ns + '\\')
                        line_out = re.sub(m, n, line_out)
                    new_file.write(line_out)

        self.constants = prefixed_constants
        self.entry_comments = ''.join(comments)
        self.remove_files += files_defining_constants

    def cleanup(self):
        """Removes unwanted files"""
        for file in self.remove_files:
            os.remove(file)

    def create_entry(self):
        """Creates the entry PHP file"""
        dst = self.dst
        form = {'namespace': self.author + '\\' + self.ns}
        constants = [
            "define('{}', {});".format(c, v)
            for c, v in self.constants.items()
        ]
        php_template = '\n'.join([
            "<?php", self.entry_comments,
            "require_once('vendor/autoload.php');\n", *constants,
            "\nnew {namespace}\\Plugin\\Plugin();"
        ])
        php = php_template.format(**form)

        with open(dst + '/' + self.ns_kepab + '.php', 'w') as entry:
            entry.write(php)

    def install(self):
        """Runs `composer install`"""
        current = os.getcwd()
        os.chdir(self.dst)
        os.system('composer install')
        os.system('composer dump-autoload --optimize')
        os.chdir(current)

    def zip_it(self):
        shutil.make_archive('../' + self.ns_kepab, 'zip', self.dst)


if sys.version_info > (3, 7):
    Standalone()
else:
    raise RuntimeError('Python 3.7+ required')
