# Contact Form 7 Extension Base

## Building a standalone version

CF7EB can be bundled with the plugin that extends it.
Asuming you have structure like this:

```
./
  cf7eb/
  cool-extension/
```

`standalone.py` is the tool to use for making a standalone version from
`cool-extension` that includes `cf7eb`.

Simple usage:
`./standalone.py -e ../cool-extension -n SUPER_COOL_EXTENSION`

This will:

- use files from `../cool-extension`,
- merging its contents with `./` into `../super-cool-extension-standalone`,
- renaming namespace to `SUPER_COOL_EXTENSION_Standalone`,
- prefixing all constants with `SUPER_COOL_EXTENSIONSTANDALONE`
- and finally running `composer install` in `../super-cool-extension-standalone`.

After that you'll have structure like this:

```
./
  cf7eb/
  cool-extension/
  super-cool-extension-standalone/
```

That's it! See more options by running `./standalone.py -h`
