const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const path = require('path')

const config = {
  mode: 'development',
  entry: ['./assets/src/js/index.js', './assets/src/css/main.scss'],
  output: {
    path: path.resolve(__dirname, 'assets'),
    filename: 'bundle.js'
  },
  watch: true,
  watchOptions: {
    ignored: /node_modules/,
    aggregateTimeout: 100,
    poll: 500
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader,
          // 'style-loader',
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: '[name].css',
      chunkFilename: '[id].css'
    })
  ]
}

module.exports = config


/*
module.exports = {
  'mode': 'production',
  'entry': 'src/index.js',
  'output': {
    'path': path.resolve(__dirname, '/assets'),
    'filename': '[name].js'
  },
  'devtool': 'source-map',
  'module': {
    'rules': [
      {
        'test': /\.js$/,
        'exclude': /node_modules/,
        'use': {
          'loader': 'babel-loader',
          'options': {
            'presets': [
              'env'
            ]
          }
        }
      },
      {
        'test': /\.scss$/,
        'use': [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },
  'plugins': [
    new MiniCssExtractPlugin({
      filename: '[name].css'
    })
  ]
}
*/
